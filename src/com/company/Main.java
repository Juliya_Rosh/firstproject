package com.company;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        HelloUser helloUser = new HelloUser();
        ReverseArguments reverseArguments = new ReverseArguments();
        RandomFigures randomFigures = new RandomFigures();
        PasswordChecking passwordChecking = new PasswordChecking();
        SumAndMult sumAndMult = new SumAndMult();
        MaxAndMin maxAndMin = new MaxAndMin();

        System.out.println("====================================================================================");
        System.out.println("\nПриветствовать любого пользователя при вводе его имени через командную строку.\n");
        helloUser.sayHello();

        System.out.println("====================================================================================");
        System.out.println("\nОтобразить в строке консоли аргументы командной строки в обратном порядке.\n");
        reverseArguments.argumentsReverse();

        System.out.println("====================================================================================");
        System.out.println("\nВывести заданное количество случайных чисел с переходом и без перехода на новую строку.\n");
        randomFigures.printRandomFigures();

        System.out.println("====================================================================================");
        System.out.println("\nВвести пароль с командной строки и сравнить его со строкой-образцом.\n");
        passwordChecking.enterPassword();

        System.out.println("====================================================================================");
        System.out.println("\nВвести целые числа как аргументы командной строки, подсчитать их сумму (произведение) и вывести результат на консоль.\n");
        sumAndMult.sumMult();

        System.out.println("====================================================================================");
        System.out.println("\nВвести с консоли n целых чисел. На консоль вывести: а дальше идет длинное задание №6, которое мне писать лень...\n");
        maxAndMin.maxMin();
    }
}

