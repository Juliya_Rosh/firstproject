package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 14.02.2017.
 */
public class HelloUser {
    public void sayHello() throws IOException {
        System.out.println("Введите Ваше имя: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        System.out.println("Привет, " + name);
    }
}
