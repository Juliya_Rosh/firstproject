package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by User1 on 17.02.2017.
 */
public class SumAndMult {
    public void sumMult() throws IOException {
        System.out.println("Введите числа через пробел.");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String  figures = reader.readLine();
        String[] args = figures.split(" ");
        int sumResult = 0;
        int multResalt = 1;
        for (String arg : args) {
            sumResult += Integer.parseInt(arg);
            multResalt *= Integer.parseInt(arg);
        }

        System.out.println("Сумма чисел: " + sumResult);
        System.out.println("Произведение чисел: " + multResalt);

    }
}
