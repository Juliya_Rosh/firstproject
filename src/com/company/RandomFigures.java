package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by User1 on 15.02.2017.
 */
public class RandomFigures {

    Random random = new Random();

    public void printRandomFigures() throws IOException {
        System.out.println("Введите количество генерируемых чисел: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int countOfFigures = Integer.parseInt(reader.readLine());

        int[] figures = new int[countOfFigures];
        for (int i = 0; i < countOfFigures; i++) {
            figures[i] = random.nextInt(1000);
        }

        for (int figure : figures) {
            System.out.println(figure);
        }
        System.out.println(Arrays.toString(figures));
    }
}
