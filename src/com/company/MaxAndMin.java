package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 17.02.2017.
 */
public class MaxAndMin {
    public void maxMin() throws IOException {
        System.out.println("Введите целое число.");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        String[] args = s.split(" ");

        System.out.println("Четные числа: ");
        for (String arg : args) {
            if (Integer.parseInt(arg) % 2 == 0) {
                System.out.print(arg + " ");
            }
        }
        System.out.println("");

        System.out.println("Нечетные числа:");
        for (String arg : args) {
            if (Integer.parseInt(arg) % 2 != 0) {
                System.out.print(arg + " ");
            }
        }
        System.out.println("");

        int min = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[0]);
        for (String arg : args) {
            if (Integer.parseInt(arg) < min) {
                min = Integer.parseInt(arg);
            }
            if (Integer.parseInt(arg) > max) {
                max = Integer.parseInt(arg);
            }
        }
        System.out.println("Минимальное число: " + min);
        System.out.println("максимальное число: " + max);

        System.out.println("\nЧисла, которые делятся на 3 или на 9");
        for (String arg : args) {
            if (Integer.parseInt(arg) % 3 == 0 || Integer.parseInt(arg) % 9 == 0) {
                System.out.print(arg + " ");
            }
        }

        System.out.println("\nЧисла, которые делятся на 5 и на 7");
        for (String arg : args) {
            if (Integer.parseInt(arg) % 5 == 0 && Integer.parseInt(arg) % 7 == 0) {
                System.out.print(arg + " ");
            }
        }


        System.out.println("\nТрехзначные числа без одинаковых десятичных цифр");
        for (String arg : args) {
            char[] figuresOfArg = arg.toCharArray();
            if (figuresOfArg.length == 3) {
                if (figuresOfArg[1] != figuresOfArg[2])
                    System.out.print(arg + " ");
            }
        }

        System.out.println("\n\"Счастливые\" числа");
        for (String arg : args) {
            char[] figuresOfArg = arg.toCharArray();
            if (figuresOfArg.length > 3 && figuresOfArg.length % 2 == 0) {
                int sumOfFirstPart = 0;
                int sumOfSecondPart = 0;
                for (int i = 0; i < figuresOfArg.length / 2; i++) {
                    sumOfFirstPart += figuresOfArg[i];
                }
                for (int i = figuresOfArg.length / 2; i < figuresOfArg.length; i++) {
                    sumOfSecondPart += figuresOfArg[i];
                }
                if (sumOfFirstPart == sumOfSecondPart) {
                    System.out.println(arg);
                }
            }
        }


        if (args.length > 2) {
            System.out.println("Элементы, равные полусуме соседних элементов: ");
            for (int i = 1; i < args.length - 1; i++) {
                if ((Integer.parseInt(args[i - 1]) + Integer.parseInt(args[i + 1])) / 2 == Double.parseDouble(args[i]))
                    System.out.println(args[i]);
            }
        } else {
            System.out.println("Недостаточно элементов для сравнения");
        }


    }
}