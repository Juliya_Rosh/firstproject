package com.company;

import com.sun.deploy.util.ArrayUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by User1 on 14.02.2017.
 */
public class ReverseArguments {
    public void argumentsReverse() throws IOException {
        System.out.println("Введите предложение: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String words = reader.readLine();
        String[] args = words.split(" ");
        String[] argsRev = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            argsRev[args.length - i - 1] = args[i];
        }
        String result = "";
        for (String s : argsRev) {
            result = result + s + " ";
        }
        System.out.println(result);


    }
}